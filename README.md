# Ping Pong Server

Ping Pong HTTP Server

Run

```
docker run --rm -it -p 8080:8080 registry.gitlab.com/narate/ping-pong
```

Test

```
$ curl 127.0.0.1:8080

{
  "message":"pong"
}

```


FROM golang:alpine as build
LABEL maintainer="Narate Ketram <koonnarate@gmail.com>"
WORKDIR /app
ADD . .
RUN go build -o ping-pong main.go

FROM alpine
WORKDIR /app
COPY --from=build /app/ping-pong /app
EXPOSE 8080
ENTRYPOINT /app/ping-pong

package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func handlerIndex(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, `Please call or send request to /ping`)
}

func handlerPing(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	fmt.Fprint(w, `{"message":"pong"}`)
}

func main() {

	ADDRESS := os.Getenv("ADDRESS")
	if ADDRESS == "" {
		ADDRESS = "0.0.0.0"
	}

	PORT := os.Getenv("PORT")
	if PORT == "" {
		PORT = "8080"
	}

	BIND := fmt.Sprintf("%s:%s", ADDRESS, PORT)

	graceful_stop := make(chan os.Signal)
	signal.Notify(graceful_stop, syscall.SIGTERM)
	signal.Notify(graceful_stop, syscall.SIGINT)

	go func() {
		sig := <-graceful_stop
		fmt.Printf("\nCaught sig: %+v\n", sig)
		fmt.Printf("Wait for 2 second to finish processing")
		time.Sleep(2 * time.Second)
		os.Exit(0)
	}()

	fmt.Printf("Running on %s:%s\n", ADDRESS, PORT)
	http.HandleFunc("/", handlerIndex)
	http.HandleFunc("/ping", handlerPing)
	log.Fatal(http.ListenAndServe(BIND, nil))
}
